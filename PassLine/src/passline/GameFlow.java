package passline;

import java.util.Scanner;

/**
 *
 * @author 1635333
 */
public class GameFlow {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PassLine game = new PassLine();
        Player user;
        ComeOutRoll outcome;
        boolean gameExit = false;
        double betAmount;
        double initialBalance;
        
        
        do {
            initialBalance = 0;
            initialBalance = Utilities.getDoubleInput("How much money did you bring with you (must be larger than 0):");
        } while (initialBalance <= 0);
        
        user = new Player(initialBalance);
        
        
        do {
        
	        do {
                    betAmount = Utilities.getDoubleInput("Bet Amount (Please ensure it is between 0 and your balance):");
	        } while(betAmount > user.getMoney() || betAmount <= 0);
	        
                int counter = 1;
	        do {
	            System.out.println("Roll " + counter + ":");
                    System.out.println("Point Value : " + game.getPoint());
	            outcome = game.PlayTurn();
	            System.out.println("Die 1: " + outcome.getDie1Value());
	            System.out.println("Die 2: " + outcome.getDie2Value());
	            System.out.println("Total: " + outcome.getTotalValue());
	            System.out.println("*************************");
                    counter++;
	        } while (outcome.getGameState() == GameState.RollAgain);
	        
	        
	       if (outcome.getGameState() == GameState.Won){ 
	            System.out.println("You Win");
	            user.bet(betAmount, true);
	       } else {
                    System.out.println("You Lost");
                    user.bet(betAmount, false);
	       }
	       
	       System.out.println("Balance remaining" + user.getMoney());
               
               if (user.getMoney() == 0 || Utilities.getIntInput("If you would like to quit, enter 1, if not, enter any other number:") == 1){
                   gameExit = true;
               }
   	
       } while(!gameExit);
        
        if (user.getMoney() > initialBalance) {
        	System.out.println("Wow Congrats, you came out with a profit!!!!!");
        } else if (user.getMoney() > 0) {
        	System.out.println("At least you didn't loose everything, meanse you can come back and win it back!");
        } else {
        	System.out.println("You can't win them all, better luck next time");
        }
       
    }
    
}
