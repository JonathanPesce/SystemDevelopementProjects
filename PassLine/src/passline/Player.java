/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

/**
 *
 * @author 1635333
 */
public class Player {
    private double money;
    
    /**
     * Initializes player with the amount of money he currently has.
     * 
     * @param startMoney Amount of money player has at the table
     */
    public Player(double startMoney) {
        money = startMoney;
    }
    
    /**
     * 
     * 
     * @param amount amount of money that the player has bet
     * @param win the outcome of the bet
     * @return the outcome of the bet
     */
    public void bet(double amount, boolean win) {
        
        if (win) {
            money += amount;
        } else {
            money -= amount;
        }
    }
    
    /**
     * @return amount of money the player currently has
     */
    public double getMoney(){
        return money;
    }
}
