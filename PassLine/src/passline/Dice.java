package passline;

/**
 *Dice is a simple class that will act as a dice, in which you determine the 
 * number of sides, that has one simple roll method.
 * 
 * @author Jonathan Pesce (Student ID: 1635333)
 */
public class Dice {
    private int sides;
    
    /**
     * Instantiates a Dice with a given number of sides. Each side of the dice 
     * will have a value ranging from 1 to the number of sides.
     * 
     * @param sides The number of sides a dice is to have
     */
    public Dice(int sides) {
        this.sides = sides;
    }
    
    /**
     * Generates a random number between 1 and the number of sides on the dice
     * @return The value which has been generated
     */
    public int roll() {
        return (int)(Math.random() * sides) + 1;
    }
}
