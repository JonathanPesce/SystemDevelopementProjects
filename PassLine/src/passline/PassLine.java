/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

/**
 *
 * @author 1635333
 */
public class PassLine {
   private int point;
   private Dice die;
   private GameState state;
   
   public PassLine(){
       die = new Dice(6);
       state = GameState.RollAgain;
       point = 0;
   }
   
   /**
    * Plays the turn of the player
    * @return the outcome of his turn
    */
   public ComeOutRoll PlayTurn() {
       int die1Roll = die.roll();
       int die2Roll = die.roll();
       GameState state = dieResults(die1Roll + die2Roll);
       return new ComeOutRoll(die1Roll,die2Roll, state);
   }  
   
   /**
    * Method checks the outcome of the sum of the two dies rolled 
    * 
    * @param numberRolled the sum of the two dies rolled
    * @return the GameState of the game
    */
   private GameState dieResults(int numberRolled) {
       if (point == 0) {
           if (numberRolled == 7 || numberRolled == 11) 
               return GameState.Won;
           if (numberRolled == 2 || numberRolled == 3 || numberRolled == 12)
               return GameState.Lost;
           point = numberRolled;
           return GameState.RollAgain;
       } 
       
        if (numberRolled == point) 
            return GameState.Won;
        if (numberRolled == 7)
            return GameState.Lost;
        return GameState.RollAgain;
   }
   
   /**
    * @return the value of the point
    */
   public int getPoint(){
       return point;
   }
   
}
