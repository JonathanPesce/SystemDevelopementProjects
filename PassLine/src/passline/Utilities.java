/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

import java.util.Scanner;

/**
 *
 * @author jonat
 */
public class Utilities {
    public static double getDoubleInput(String promptMessage) {
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println(promptMessage);
            if (scanner.hasNextDouble()){
                 return scanner.nextDouble();
            } else {
                System.out.println("Sorry that is not a valid number");
                scanner.next();
            }
        } while (true);
    }
    
    public static int getIntInput(String promptMessage) {
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println(promptMessage);
            if (scanner.hasNextInt()){
                 return scanner.nextInt();
            } else {
                System.out.println("Sorry that is not a valid number");
                scanner.next();
            }
        } while (true);
    }
}
