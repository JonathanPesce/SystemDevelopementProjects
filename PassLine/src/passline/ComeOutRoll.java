/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

/**
 *This class holds all the information for an outcome of a roll
 * 
 * @author 1635333
 */
public class ComeOutRoll {
    private int die1Value;
    private int die2Value;
    private int totalValue;
    private GameState state;
    
    /**
     * 
     * @param die1Value the value rolled of the first die
     * @param die2Value the value rolled on the second die
     * @param state the end result of the roll, if you won, lost or need to roll again
     */
    public ComeOutRoll(int die1Value, int die2Value, GameState state){
        this.die1Value = die1Value;
        this.die2Value = die2Value;
        this.totalValue = die1Value + die2Value;
        this.state = state;
    }
    
    /**
     * @return value of the first die
     */
    public int getDie1Value() {
        return die1Value;
    }
    
    /**
     * @return value of the second die 
     */
    public int getDie2Value() {
        return die2Value;
    }
    
    /**
     * @return the total value of the first and second die
     */
    public int getTotalValue() {
        return totalValue;
    }
    
    /**
     * @return the game sate after the roll
     */
    public GameState getGameState() {
        return state;
    }
}
