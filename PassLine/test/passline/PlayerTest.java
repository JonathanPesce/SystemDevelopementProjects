/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1635333
 */
public class PlayerTest {
    
    public PlayerTest() {
    }

    /**
     * Test of bet method if the user has one the bet
     */
    @Test
    public void testBetWon() {
        double amount = 5.0;
        boolean win = true;
        double startMoney = 20.0; 
        
        Player instance = new Player(startMoney);
        instance.bet(amount, win);
       
        
        assertEquals(startMoney + amount, instance.getMoney(), 0.01);
    }
    
     /**
     * Test of bet method if the user has lost his bet
     */
    @Test
    public void testBetLost() {
        double amount = 5.0;
        boolean win = false;
        double startMoney = 20.0; 
        
        Player instance = new Player(startMoney);
        instance.bet(amount, win);
       
        
        assertEquals(startMoney - amount, instance.getMoney(), 0.01);
    }
    
}
