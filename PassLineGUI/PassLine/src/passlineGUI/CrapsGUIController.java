/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passlineGUI;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import passline.*;


/**
 *
 * @author 1635333
 */
public class CrapsGUIController implements Initializable {
    @FXML
    private Button rollDice, sumbitStartMoney;
    @FXML
    private Text gameState, balance, die1, die2, point;
    @FXML 
    private TextField betAmount, startMoney;

    private Player user;
    private Dice die;
    private PassLine game;
    private ComeOutRoll comeOut;
    
            
    
    @FXML
    private void rollDice(ActionEvent event) {
        betAmount.setEditable(false);
        rollDice.setText("Roll Again");
        comeOut = game.PlayTurn();
        checkOutCome();
        updateGUI();
    }
    
    private void updateGUI(){
        balance.setText("Balance: " + user.getMoney());
        gameState.setText("Game State: " + comeOut.getGameState());
        die1.setText(Integer.toString(comeOut.getDie1Value()));
        die2.setText(Integer.toString(comeOut.getDie2Value()));
        point.setText("Point: " + game.getPoint());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        die = new Dice(6);
        comeOut = new ComeOutRoll(1, 1, GameState.RollAgain);
        rollDice.setDisable(true);
        betAmount.setEditable(false);
    }    
    
    @FXML
    private void instantiateUser(ActionEvent event) {
        user = new Player(Double.parseDouble(startMoney.getText()));
        sumbitStartMoney.setDisable(true);
        startMoney.setEditable(false);
        resetBoard();
        this.updateGUI();
    }
    
    private void resetBoard() {
        game = new PassLine();
        rollDice.setText("Start New Game");
        betAmount.setEditable(true);
        rollDice.setDisable(false);
        point.setText("Point: " + game.getPoint());
    }
    
    private void checkOutCome(){
        if (comeOut.getGameState() == GameState.Won) {
            user.bet(Double.parseDouble(betAmount.getText()), true);
            resetBoard();
        } else if (comeOut.getGameState() == GameState.Lost) {
            user.bet(Double.parseDouble(betAmount.getText()), false);
        }
         resetBoard();
    }
}
